`timescale 1ns / 1ps

module select_display_value (
	input clk,
	input hs,
	input [31:0] points,
	input [31:0] highscore,
	output reg [31:0] val
	);

	initial begin	
		val = points;
	end

	always @ (posedge clk or posedge hs) begin
		if (hs) begin
			val <= highscore;
		end
		else begin
			val <= points;
		end
	end

endmodule
