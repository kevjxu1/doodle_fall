`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:10:09 05/19/2016 
// Design Name: 
// Module Name:    debouncer 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Debouncer for any buttons such as the reset button on the board, to restart
//					 the game.
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module debouncer(
	input clk,
	input button,
	output new_button
    );

reg [15:0] counter;
reg button_state_reg = 0;

always @ (posedge clk) begin
	if(button == 0)
	begin
		counter <= 0;
		button_state_reg <=0;
	end
	else
	begin
		counter <= counter + 1'b1;
		if(counter == 16'hffff)
		begin
			button_state_reg<= 1;
			counter <= 0;
		end
	end
end

assign new_button = button_state_reg;

endmodule
